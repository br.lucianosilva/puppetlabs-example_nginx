# Class: example_nginx
# ===========================
#
# Full description of class example_nginx here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'example_nginx':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2018 Your name here, unless otherwise noted.
#
class example_nginx::config {
$var_nginx = $::os['family']? {
	'Ubuntu' => 'nginx',
	default => 'ngnix',
}
	package {$var_nginx:
		ensure => present,
	}
	File {
		owner => "root",
		group => "root",
		mode => "0644",
	}
	file {'/usr/share/nginx/html/index.html':
		ensure => file,
		source => 'puppet:///modules/example_nginx/nginx/index.html',
		require => Package[$var_nginx],
	}
	service {$var_nginx:
		ensure => running,
		enable => true,
		subscribe => File['/usr/share/nginx/html/index.html'],
	}
}
